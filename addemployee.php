<?php
include 'db.php';
include 'header.php';
?>
   <form class="needs-validation" novalidate method="POST" action="includeemployee.php">
        <div class="form-row">
          <div class="col-md-4 mb-3">
            <label for="city">First Name</label>
            <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name" required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
          <div class="col-md-4 mb-3">
            <label for="price">Last Name</label>
            <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name" required>
            <div class="valid-feedback">
              Looks good!
            </div>
          </div>
         </div>
        <div class="form-row">     
         <div class="col-md-4 mb-3">
           <button class="btn btn-primary" type="submit">Add Employee</button>   
          </div>
        </div>           
    </form>
    
	    <script src="form-validation.js" defer></script>
<?php
include 'footer.php'
?>