<?php
include 'db.php';
include 'header.php';
?>
  <div id="course" class="container">
		<div class="row">
		<div class="col-12">
			<table class="table table-hover table-striped" id="cursos">
					<thead class="thead-dark">
						<tr>
							<th>Lot ID</th>
							<th class="text-right"> Profit</th>
						</tr>
					</thead>
					<tbody>

    					<?php 
                        $query = "SELECT LID, SUM(AmountPaid) AS TOTAL FROM Car GROUP BY LID ORDER BY TOTAL DESC";
                        $query_amountpaid = mysqli_query($conexion, $query);
                        
                                  if ( !$query_amountpaid) {
                                        echo "error !!!";
                                        $error_number = mysqli_error( $conexion );
                                        $error_message = mysqli_error( $conexion );
                                        file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                                        http_response_code( 500 );
                                        exit(1);
                                  }                                 
							while($linha = mysqli_fetch_array($query_amountpaid)){
								echo '<tr><td >'.$linha['LID'].'</td>';
								echo '<td class="text-right">$'.$linha['TOTAL'].'</td></tr>';
    						}
    					?>
							
					</tbody>
			</table>
		</div>

		</div> <!--row DIV -->
		
	</div>  <!-- container DIV -->
	
<?php
include 'footer.php'
?>