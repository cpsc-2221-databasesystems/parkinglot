<?php
include 'db.php';
include 'header.php';
?>
  <div id="course" class="container">
    <h1>About</h1>
      <h3 class="mt-2">Project Title: Car Parking Management System</h3>
          <p>This project was developed during CPSC-2221-Database System course.</p>
          <p>
            The hypothetical situation considered a big company with several car parking lots on its premises.
            The goal was to create a database for the management and ticketing of these car parking lots.
          </p>
          <p>Its main purpose was to apply all database structure concepts learned during the term in the following steps </p>
            <ol>
              <li> Planning and system analysis </li>
              <li> ER/EER drawing </li>
              <li> Mapping ER/EER diagram to a relational schema </li>
              <li> Normalize the database up to Boyce–Codd normal form </li>
              <li> Create the database tables using SQL DDL (Data Definition Language) and insert some mock values.</li>
              <li> Implement a front-end application with SQL queries to the database </li>
            </ol>
      <h3 class="mt-2"> Planning and system analysis</h3>
        <h4> The data requirements are as follows &#58</h4>
          <ul>
          	<li> A parking lot is identified by a unique id and can have one or more blocks/ parking grounds.</li>
          	<li> A block has many parking stalls and has a block code.</li>
          	<li> A stall has number, size(small cars/large cars), and type (for handicaps, for bicycles, reserved for police)</li>
          	<li> A customer can have weekly, monthly or a yearly pass.</li>
          	<li> If a customer does not have a pass, they can pay on daily basis for which vehicle’s license plate number and duration of parking is required.</li>
          	<li> A regular pass has its total cost and duration of the pass.</li>
          	<li> For the daily customer a receipt is provided which shows the cost, duration and license plate of the parked vehicle.</li>
          	<li> If a car parked in a stall without a valid pass, a ticket is issues for the car.</li>
          	<li> The ticket includes: date, time, info about the car, stall number, fine amount, the employee ID of the person who issued the ticket.</li>
          </ul>
          <h5> Assumptions about cardinality and participations </h5>
            <ul>
            	<li> Everyone's given a receipt (regardless of pass type).</li>
            	<li> Passholders will pay the difference of their pass expiry and current time (this is done in the business logic).</li>
            	<li> Passes are tied to cars (i.e licence plate).</li>
            	<li> Many passes for the same car per lot.</li>
            	<li> Passes (weekly, monthly, yearly), are handled in the business logic. The database only stores when a pass was purchased and when it expires</li>
            </ul>
            
     <h3 class="mt-2"> ER/EER drawing</h3>
        <img alt="image of the ER/ERR diagram" src='img/EER_Modeling_Diagram.svg'>
        
     <h3 class="mt-2"> Mapping ER/EER diagram to a relational schema </h3>
        <img alt="Mapping ER/EER diagram to a relational schema" src='img/ER_Model_Mapping.png'>

     <h3 class="mt-2">  Normalize the database up to Boyce–Codd normal form </h3>
        <h4> Normalization </h4>
          <h5> First normal form </h5>
            <p> Already in first normal form because there are no composite nor any multi-valued attributes. </p>
          <h5> Second normal form </h5>
            <p> Already in second normal form because all non-prime attributes are fully functional on the primary key. </p>
          <h5> Third normal form </h5>
            <p> Already in third normal form because there are no non-prime attributes that are transitively dependent on prime attribute. </p>
          <h5> Boyce–Codd normal form </h5>
            <p>Already in BCNF because the database is already in the third normal form and there are no candidate keys. </p>

     <h3 class="mt-2"> Database diagram </h3>
        <img alt="Database diagram" src='img/ParkingAdmDiagram.svg'>
        
     <h3 class="mt-0"> SQL DDL and mock values insertion </h3>
       <pre>
        <code>
    CREATE TABLE `Car` (
      `LicensePlate` varchar(7) NOT NULL,
      `EntryTimestamp` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
      `ExitTimestamp` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
      `Make` varchar(25) NOT NULL,
      `Model` varchar(25) NOT NULL,
      `LID` decimal(10,0) DEFAULT NULL,
      `BlockCode` char(1) NOT NULL,
      `StallNumber` decimal(10,0) DEFAULT NULL,
      `AmountPaid` decimal(5,2) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    

    INSERT INTO `Car` (`LicensePlate`, `EntryTimestamp`, `ExitTimestamp`, `Make`, `Model`, `LID`, `BlockCode`, `StallNumber`, `AmountPaid`) VALUES
    ('123ABC', '2019-07-21 09:00:00', '2019-07-21 11:00:00', 'Honda', 'Civic', '1', 'A', '1', '7.45'),
    ('294SJD', '2019-08-22 19:00:00', '2019-08-22 20:40:00', 'Toyota', 'Camry', '1', 'B', '3', '7.45'),
    ('BEO148', '2019-01-25 03:27:22', '2019-05-14 06:24:59', 'Ford', 'Tourneo Connect', '3', 'B', '7', '71.68'),
    ('BOO898', '2019-03-09 01:03:14', '2019-06-01 12:18:44', 'Hyundai', 'Tucson', '3', 'C', '5', '55.48'),
    ('BPV359', '2019-03-22 14:41:53', '2019-07-16 10:03:07', 'Volvo', 'V40', '3', 'B', '8', '76.10'),
    ('BXA996', '2019-01-21 04:23:23', '2019-03-28 08:55:54', 'Mini', 'Mini', '3', 'A', '6', '43.47'),
    ('DEH535', '2019-04-06 02:09:07', '2019-06-02 13:47:18', 'Toyota', 'C-HR', '3', 'A', '9', '37.77'),
    ('DHU422', '2019-06-24 22:51:50', '2019-06-30 07:12:46', 'Volkswagen', 'Golf SV', '3', 'B', '1', '128.35'),
    ('DUP991', '2019-07-08 03:13:14', '2019-07-25 11:42:35', 'Ford', 'Kuga', '1', 'B', '1', '11.40'),
    ('EEJ524', '2019-02-07 03:09:49', '2019-03-12 22:45:20', 'Volvo', 'XC40', '1', 'A', '2', '22.19'),
    ('EIH226', '2019-04-02 23:19:12', '2019-06-27 14:56:23', 'Ford', 'S-MAX', '3', 'B', '2', '56.28'),
    ('EXH723', '2019-04-09 09:24:01', '2019-04-17 14:02:41', 'Jeep', 'Compass', '2', 'C', '8', '5.38'),
    ('FFR372', '2019-01-16 07:13:30', '2019-04-26 06:08:34', 'Fiat', '500C', '1', 'A', '8', '65.65'),
    ('FSO161', '2019-05-09 17:55:14', '2019-06-30 08:52:35', 'Suzuki', 'Celerio', '2', 'C', '3', '33.92'),
    ('GGM126', '2019-01-11 02:58:47', '2019-06-16 14:35:15', 'Jeep', 'Renegade', '2', 'B', '6', '102.80'),
    ('HLB221', '2019-07-24 05:50:03', '2019-07-25 06:57:16', 'Fiat', '500L', '1', 'A', '9', '25.12'),
    ('HUD734', '2019-07-09 22:48:32', '2019-07-12 14:28:37', 'Mercedes Benz', 'A-Class', '3', 'A', '1', '63.67'),
    ('IHL951', '2019-07-24 02:34:40', '2019-07-25 00:15:59', 'Fiat', '500', '1', 'C', '4', '21.69'),
    ('INO223', '2019-06-22 11:00:20', '2019-07-17 21:27:45', 'Jeep', 'Renegade', '2', 'B', '8', '16.71'),
    ('IOT617', '2019-01-13 23:21:38', '2019-01-16 10:11:09', 'Ford', 'Galaxy', '3', 'A', '3', '58.83'),
    ('IWK595', '2019-01-27 10:35:42', '2019-03-19 19:20:14', 'Ford', 'Grand C-MAX', '2', 'B', '10', '33.72'),
    ('JGT785', '2019-02-08 15:33:33', '2019-06-23 01:53:21', 'Ford', 'EcoSport', '3', 'C', '5', '88.31'),
    ('JHJ623', '2019-02-15 21:04:17', '2019-06-22 18:15:43', 'Volkswagen', 'Touran', '3', 'B', '9', '83.35'),
    ('KTB462', '2019-03-03 21:33:52', '2019-03-12 10:19:42', 'Suzuki', 'Swift', '3', 'B', '7', '5.58'),
    ('KZC554', '2019-01-16 20:56:11', '2019-07-08 21:45:45', 'Suzuki', 'Vitara', '2', 'B', '2', '113.67'),
    ('LAM968', '2019-02-19 02:14:56', '2019-03-19 02:36:47', 'Nissan', 'Micra', '2', 'B', '8', '18.38'),
    ('LCY967', '2019-05-13 21:30:47', '2019-07-17 09:46:23', 'Kia', 'ProCeed', '2', 'C', '6', '42.39'),
    ('LPV629', '2019-05-24 13:17:53', '2019-06-05 09:24:27', 'Mazda', 'Mazda6', '3', 'C', '3', '7.78'),
    ('LRZ377', '2019-03-02 16:51:44', '2019-05-24 20:43:44', 'Suzuki', 'Vitara', '2', 'A', '9', '54.62'),
    ('LYK468', '2019-01-09 05:23:37', '2019-05-25 07:57:33', 'Nissan', 'Juke', '2', 'A', '1', '89.41'),
    ('LYS912', '2019-03-25 20:20:51', '2019-07-24 10:22:22', 'Fiat', 'Qubo', '3', 'B', '4', '79.24'),
    ('MVZ397', '2019-05-31 13:50:36', '2019-07-13 06:53:40', 'Nissan', 'Micra', '1', 'A', '10', '28.06'),
    ('NTP232', '2019-05-04 12:12:23', '2019-06-05 02:53:04', 'BMW', 'i3', '3', 'A', '9', '20.77'),
    ('NWM976', '2019-07-15 05:28:51', '2019-07-18 12:25:27', 'Toyota', 'Prius+', '1', 'B', '8', '78.94'),
    ('OAV348', '2019-02-06 03:58:07', '2019-04-09 01:52:19', 'Suzuki', 'Vitara', '1', 'B', '6', '40.66'),
    ('OCS444', '2019-05-06 18:12:38', '2019-06-25 01:30:52', 'Fiat', '500C', '3', 'C', '7', '32.40'),
    ('OOK337', '2019-04-02 22:35:25', '2019-04-17 09:18:31', 'Jeep', 'Renegade', '1', 'A', '8', '9.49'),
    ('PAN318', '2019-03-18 16:36:53', '2019-04-08 04:33:33', 'Hyundai', 'i20', '3', 'A', '3', '13.47'),
    ('PEC584', '2019-04-15 04:56:05', '2019-07-22 17:14:49', 'BMW', 'i3', '1', 'B', '10', '64.73'),
    ('PGK782', '2019-06-27 03:21:34', '2019-07-06 06:10:35', 'Fiat', 'Panda', '3', 'A', '1', '5.99'),
    ('POU178', '2019-02-27 19:29:51', '2019-04-17 02:48:42', 'Suzuki', 'S-Cross', '1', 'A', '9', '31.71'),
    ('PWB214', '2019-03-21 10:16:13', '2019-05-21 03:52:42', 'Hyundai', 'ix20', '3', 'A', '9', '39.91'),
    ('QFM573', '2019-03-15 10:25:24', '2019-07-02 12:27:20', 'Mercedes Benz', 'B-Class', '2', 'B', '2', '71.68'),
    ('QHV949', '2019-05-26 10:20:43', '2019-05-31 00:19:02', 'Toyota', 'C-HR', '2', 'B', '6', '109.97'),
    ('QLE399', '2019-06-28 01:37:40', '2019-07-20 11:58:22', 'Hyundai', 'i10', '1', 'C', '7', '14.74'),
    ('QWJ123', '2019-03-21 11:10:32', '2019-07-20 18:16:59', 'Nissan', 'Juke', '1', 'C', '8', '79.70'),
    ('RGO428', '2019-04-15 10:12:59', '2019-05-05 21:08:23', 'Ford', 'EcoSport', '3', 'B', '8', '13.44'),
    ('RUQ686', '2019-06-15 23:38:44', '2019-06-29 20:50:43', 'Volkswagen', 'Touran', '2', 'A', '1', '9.12'),
    ('S3JS23', '2019-09-23 17:00:00', '2019-09-23 18:02:00', 'Honda', 'Fit', '1', 'C', '1', '7.45'),
    ('SAE423', '2019-07-11 11:37:23', '2019-07-16 14:38:35', 'Ford', 'Grand Tourneo Connect', '1', 'C', '10', '123.02'),
    ('SQL881', '2019-04-05 11:58:50', '2019-07-19 09:46:12', 'Volkswagen', 'Golf SV', '2', 'B', '2', '68.93'),
    ('TID841', '2019-01-25 06:35:31', '2019-03-09 17:30:01', 'Kia', 'Stonic', '2', 'B', '1', '28.55'),
    ('TNK756', '2019-03-25 16:58:05', '2019-06-12 02:13:51', 'Ford', 'Mondeo', '1', 'A', '8', '51.51'),
    ('TXP886', '2019-06-21 02:55:23', '2019-07-20 18:04:12', 'Fiat', '500X', '3', 'C', '9', '19.47'),
    ('TYG329', '2019-02-05 01:17:50', '2019-05-25 21:20:02', 'Volkswagen', 'T-ROC', '1', 'B', '10', '72.14'),
    ('UNP838', '2019-01-11 00:40:17', '2019-03-17 01:31:26', 'Suzuki', 'Celerio', '2', 'C', '1', '42.71'),
    ('VDQ913', '2019-04-01 21:57:35', '2019-06-25 12:55:09', 'Hyundai', 'i10', '2', 'A', '4', '55.61'),
    ('VJV725', '2019-02-26 19:32:17', '2019-04-01 04:59:49', 'Ford', 'Grand Tourneo Connect', '1', 'A', '5', '21.92'),
    ('VPE412', '2019-01-17 09:00:37', '2019-06-25 20:26:48', 'Ford', 'S-MAX', '3', 'C', '6', '104.76'),
    ('XMV452', '2019-06-29 01:47:45', '2019-07-01 00:04:36', 'Audi', 'A3', '1', 'B', '4', '46.28'),
    ('YHG672', '2019-01-25 13:34:16', '2019-01-25 20:20:46', 'Kia', 'ProCeed', '3', 'A', '10', '6.78'),
    ('YQS766', '2019-02-14 22:17:27', '2019-06-28 17:00:45', 'Hyundai', 'ix20', '2', 'A', '3', '87.88'),
    ('ZLW134', '2019-02-28 13:48:39', '2019-04-06 12:21:23', 'Ford', 'Kuga', '1', 'C', '2', '24.25');
    
    CREATE TABLE `Employee` (
      `EID` int(11) NOT NULL,
      `FirstName` varchar(50) NOT NULL,
      `LastName` varchar(50) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    INSERT INTO `Employee` (`EID`, `FirstName`, `LastName`) VALUES
    (1, 'Joe', 'Smith'),
    (2, 'Jane', 'Doe'),
    (3, 'John', 'Hanris'),
    (8, 'Andrew', 'Kooler Kan'),
    (10, 'Daniel', 'Liang'),
    (11, 'Richard', 'Ghere'),
    (12, 'Anthony', 'Hopkins'),
    (13, 'Cristopher', 'Reeve');
    
    CREATE TABLE `Fine` (
      `FID` int(11) NOT NULL,
      `Description` varchar(200) DEFAULT NULL,
      `AmountDue` decimal(5,2) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    INSERT INTO `Fine` (`FID`, `Description`, `AmountDue`) VALUES
    (1, 'Failure to renew parking reservation', '35.00'),
    (2, 'Parked in police stall', '125.00'),
    (3, 'Parking in handicap stall without handicap sign', '200.00');
    
    CREATE TABLE `Lot` (
      `LID` decimal(10,0) NOT NULL,
      `Street` varchar(25) NOT NULL,
      `City` varchar(25) NOT NULL,
      `Province` char(2) NOT NULL,
      `PostCode` char(6) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    INSERT INTO `Lot` (`LID`, `Street`, `City`, `Province`, `PostCode`) VALUES
    ('1', '777 Brockton Avenue', 'Vancouver', 'BC', 'V5H1S0'),
    ('2', '30 Memorial Drive', 'Vancouver', 'BC', 'V5H1S1'),
    ('3', '250 Hartford Avenue', 'Vancouver', 'BC', 'V5H1S2'),
    ('4', '700 Oak Street', 'Vancouver', 'BC', 'V5H1S3'),
    ('5', '664 Parkhurst Rd', 'Vancouver', 'BC', 'V5H1S4'),
    ('6', '591 Memorial Dr', 'Vancouver', 'BC', 'V5H1S5'),
    ('7', '55 Brooksby Village Way', 'Vancouver', 'BC', 'V5H1S6'),
    ('8', '137 Teaticket Hwy', 'Vancouver', 'BC', 'V5H1S7'),
    ('9', '42 Fairhaven Commons Way', 'Vancouver', 'BC', 'V5H1S9');
    
    CREATE TABLE `Lot_Block` (
      `LID` decimal(10,0) NOT NULL,
      `BlockCode` char(1) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    INSERT INTO `Lot_Block` (`LID`, `BlockCode`) VALUES
    ('1', 'A'),
    ('1', 'B'),
    ('1', 'C'),
    ('2', 'A'),
    ('2', 'B'),
    ('2', 'C'),
    ('3', 'A'),
    ('3', 'B'),
    ('3', 'C');
    
    CREATE TABLE `Lot_Block_Stall` (
      `LID` decimal(10,0) NOT NULL,
      `BlockCode` char(1) NOT NULL,
      `StallNumber` decimal(10,0) NOT NULL,
      `SSize` char(1) NOT NULL,
      `Stype` char(1) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    INSERT INTO `Lot_Block_Stall` (`LID`, `BlockCode`, `StallNumber`, `SSize`, `Stype`) VALUES
    ('1', 'A', '1', 'M', 'P'),
    ('1', 'A', '2', 'M', 'P'),
    ('1', 'A', '3', 'M', 'P'),
    ('1', 'A', '4', 'M', 'P'),
    ('1', 'A', '5', 'M', 'P'),
    ('1', 'A', '6', 'M', 'P'),
    ('1', 'A', '7', 'M', 'P'),
    ('1', 'A', '8', 'M', 'P'),
    ('1', 'A', '9', 'S', 'M'),
    ('1', 'A', '10', 'M', 'P'),
    ('1', 'B', '1', 'M', 'P'),
    ('1', 'B', '2', 'M', 'P'),
    ('1', 'B', '3', 'M', 'P'),
    ('1', 'B', '4', 'M', 'P'),
    ('1', 'B', '5', 'M', 'P'),
    ('1', 'B', '6', 'M', 'P'),
    ('1', 'B', '7', 'M', 'P'),
    ('1', 'B', '8', 'M', 'P'),
    ('1', 'B', '9', 'S', 'M'),
    ('1', 'B', '10', 'M', 'P'),
    ('1', 'C', '1', 'M', 'P'),
    ('1', 'C', '2', 'M', 'P'),
    ('1', 'C', '3', 'M', 'P'),
    ('1', 'C', '4', 'M', 'P'),
    ('1', 'C', '5', 'M', 'P'),
    ('1', 'C', '6', 'M', 'P'),
    ('1', 'C', '7', 'M', 'P'),
    ('1', 'C', '8', 'M', 'P'),
    ('1', 'C', '9', 'S', 'M'),
    ('1', 'C', '10', 'M', 'P'),
    ('2', 'A', '1', 'M', 'P'),
    ('2', 'A', '2', 'M', 'P'),
    ('2', 'A', '3', 'M', 'P'),
    ('2', 'A', '4', 'M', 'P'),
    ('2', 'A', '5', 'M', 'P'),
    ('2', 'A', '6', 'M', 'P'),
    ('2', 'A', '7', 'M', 'P'),
    ('2', 'A', '8', 'M', 'P'),
    ('2', 'A', '9', 'S', 'M'),
    ('2', 'A', '10', 'M', 'P'),
    ('2', 'B', '1', 'M', 'P'),
    ('2', 'B', '2', 'M', 'P'),
    ('2', 'B', '3', 'M', 'P'),
    ('2', 'B', '4', 'M', 'P'),
    ('2', 'B', '5', 'M', 'P'),
    ('2', 'B', '6', 'M', 'P'),
    ('2', 'B', '7', 'M', 'P'),
    ('2', 'B', '8', 'M', 'P'),
    ('2', 'B', '9', 'S', 'M'),
    ('2', 'B', '10', 'M', 'P'),
    ('2', 'C', '1', 'M', 'P'),
    ('2', 'C', '2', 'M', 'P'),
    ('2', 'C', '3', 'M', 'P'),
    ('2', 'C', '4', 'M', 'P'),
    ('2', 'C', '5', 'M', 'P'),
    ('2', 'C', '6', 'M', 'P'),
    ('2', 'C', '7', 'M', 'P'),
    ('2', 'C', '8', 'M', 'P'),
    ('2', 'C', '9', 'S', 'M'),
    ('2', 'C', '10', 'M', 'P'),
    ('3', 'A', '1', 'M', 'P'),
    ('3', 'A', '2', 'M', 'P'),
    ('3', 'A', '3', 'M', 'P'),
    ('3', 'A', '4', 'M', 'P'),
    ('3', 'A', '5', 'M', 'P'),
    ('3', 'A', '6', 'M', 'P'),
    ('3', 'A', '7', 'M', 'P'),
    ('3', 'A', '8', 'M', 'P'),
    ('3', 'A', '9', 'S', 'M'),
    ('3', 'A', '10', 'M', 'P'),
    ('3', 'B', '1', 'M', 'P'),
    ('3', 'B', '2', 'M', 'P'),
    ('3', 'B', '3', 'M', 'P'),
    ('3', 'B', '4', 'M', 'P'),
    ('3', 'B', '5', 'M', 'P'),
    ('3', 'B', '6', 'M', 'P'),
    ('3', 'B', '7', 'M', 'P'),
    ('3', 'B', '8', 'M', 'P'),
    ('3', 'B', '9', 'S', 'M'),
    ('3', 'B', '10', 'M', 'P'),
    ('3', 'C', '1', 'M', 'P'),
    ('3', 'C', '2', 'M', 'P'),
    ('3', 'C', '3', 'M', 'P'),
    ('3', 'C', '4', 'M', 'P'),
    ('3', 'C', '5', 'M', 'P'),
    ('3', 'C', '6', 'M', 'P'),
    ('3', 'C', '7', 'M', 'P'),
    ('3', 'C', '8', 'M', 'P'),
    ('3', 'C', '9', 'S', 'M'),
    ('3', 'C', '10', 'M', 'P');
    
    CREATE TABLE `Ticket` (
      `TID` int(11) NOT NULL,
      `TTimestamp` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
      `EntryTimestamp` timestamp NOT NULL DEFAULT '1970-01-01 00:00:01',
      `LID` decimal(10,0) DEFAULT NULL,
      `BlockCode` char(1) NOT NULL,
      `StallNumber` decimal(10,0) DEFAULT NULL,
      `Issuer_EID` int(11) DEFAULT NULL,
      `FID` int(11) DEFAULT NULL,
      `LicensePlate` varchar(7) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    
    INSERT INTO `Ticket` (`TID`, `TTimestamp`, `EntryTimestamp`, `LID`, `BlockCode`, `StallNumber`, `Issuer_EID`, `FID`, `LicensePlate`) VALUES
    (1, '2019-07-21 23:15:00', '2019-07-21 09:00:00', '1', 'A', '1', 1, 1, '123ABC'),
    (2, '2019-07-22 10:18:00', '2019-08-22 19:00:00', '1', 'A', '2', 2, 2, '294SJD'),
    (3, '2019-07-23 18:00:00', '2019-09-23 17:00:00', '1', 'B', '3', 3, 3, 'S3JS23');
    
    ALTER TABLE `Car`
      ADD PRIMARY KEY (`LicensePlate`,`EntryTimestamp`),
      ADD KEY `LID` (`LID`,`BlockCode`,`StallNumber`);

    ALTER TABLE `Employee`
      ADD PRIMARY KEY (`EID`);
    
    ALTER TABLE `Fine`
      ADD PRIMARY KEY (`FID`);
    
    ALTER TABLE `Lot`
      ADD PRIMARY KEY (`LID`);
    
    ALTER TABLE `Lot_Block`
      ADD PRIMARY KEY (`LID`,`BlockCode`);
    
    ALTER TABLE `Lot_Block_Stall`
      ADD PRIMARY KEY (`LID`,`BlockCode`,`StallNumber`);
    
    ALTER TABLE `Ticket`
      ADD PRIMARY KEY (`TID`),
      ADD KEY `LID` (`LID`,`BlockCode`,`StallNumber`),
      ADD KEY `Issuer_EID` (`Issuer_EID`),
      ADD KEY `FID` (`FID`),
      ADD KEY `LicensePlate` (`LicensePlate`,`EntryTimestamp`);
    
    ALTER TABLE `Employee`
      MODIFY `EID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
    
    ALTER TABLE `Fine`
      MODIFY `FID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
    
    ALTER TABLE `Ticket`
      MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
    
    ALTER TABLE `Car`
      ADD CONSTRAINT `Car_ibfk_1` FOREIGN KEY (`LID`) REFERENCES `Lot` (`LID`) ON DELETE CASCADE,
      ADD CONSTRAINT `Car_ibfk_2` FOREIGN KEY (`LID`,`BlockCode`) REFERENCES `Lot_Block` (`LID`, `BlockCode`) ON DELETE CASCADE,
      ADD CONSTRAINT `Car_ibfk_3` FOREIGN KEY (`LID`,`BlockCode`,`StallNumber`) REFERENCES `Lot_Block_Stall` (`LID`, `BlockCode`, `StallNumber`) ON DELETE CASCADE;
    
    ALTER TABLE `Lot_Block`
      ADD CONSTRAINT `Lot_Block_ibfk_1` FOREIGN KEY (`LID`) REFERENCES `Lot` (`LID`) ON DELETE CASCADE;
    
    ALTER TABLE `Lot_Block_Stall`
      ADD CONSTRAINT `Lot_Block_Stall_ibfk_1` FOREIGN KEY (`LID`) REFERENCES `Lot` (`LID`) ON DELETE CASCADE,
      ADD CONSTRAINT `Lot_Block_Stall_ibfk_2` FOREIGN KEY (`LID`,`BlockCode`) REFERENCES `Lot_Block` (`LID`, `BlockCode`) ON DELETE CASCADE;

    ALTER TABLE `Ticket`
      ADD CONSTRAINT `Ticket_ibfk_1` FOREIGN KEY (`LID`) REFERENCES `Lot` (`LID`) ON DELETE CASCADE,
      ADD CONSTRAINT `Ticket_ibfk_2` FOREIGN KEY (`LID`,`BlockCode`) REFERENCES `Lot_Block` (`LID`, `BlockCode`) ON DELETE CASCADE,
      ADD CONSTRAINT `Ticket_ibfk_3` FOREIGN KEY (`LID`,`BlockCode`,`StallNumber`) REFERENCES `Lot_Block_Stall` (`LID`, `BlockCode`, `StallNumber`) ON DELETE CASCADE,
      ADD CONSTRAINT `Ticket_ibfk_4` FOREIGN KEY (`Issuer_EID`) REFERENCES `Employee` (`EID`) ON DELETE CASCADE,
      ADD CONSTRAINT `Ticket_ibfk_5` FOREIGN KEY (`FID`) REFERENCES `Fine` (`FID`) ON DELETE CASCADE,
      ADD CONSTRAINT `Ticket_ibfk_6` FOREIGN KEY (`LicensePlate`,`EntryTimestamp`) REFERENCES `Car` (`LicensePlate`, `EntryTimestamp`) ON DELETE CASCADE;
    COMMIT;

        </code>
      </pre>
  </div>
<?php
include 'footer.php'
?>