<?php
include 'db.php';
include 'header.php';
?>

<div id="terms" class="container">
    <h2 class="ml-1 text-center"> Terms and Conditions </h2>
        <p class="ml-1 text-justify"> Database Park does not give any warranty or other assurance as to the operation, quality or functionality of the site.
        Access to the site may be interrupted, restricted or delayed for any reason.</p>
        <p class="ml-1 text-justify"> Database Park also does not give any warranty or other assurance as to the content of the material appearing on the
        site, its accuracy, completeness, timelessness or fitness for any particular purpose.</p>
        <p class="ml-1 text-justify"> To the full extent permissible by law, Database Park disclaims all responsibility for any damages or losses (including,
        without limitation, financial loss, damages for loss in business projects, loss of profits or other consequential losses) arising in contract, tort or otherwise from the use of or inability to use the Site or any material appearing on Database Park, or from any action or decision taken as a result of using Database Park or any such material. Database Park contains links to external sites. Database Park is not responsible for and has no control over the content of such sites. Information on Database Park, or available via hypertext link from Database Park, is made available without responsibility on the part of Database Park. Database Park disclaims all responsibility and liability (including for negligence) in relation to information on or accessible from Database Park.</p>
        <p class="ml-1 text-justify"> In the event that the parking rates posted on Database Park do not match the rates charged by the facilities parking equipment,
        the rate charged at the facility by the equipment will be deemed correct.</p>  
</div>	
<?php
include 'footer.php'
?>
