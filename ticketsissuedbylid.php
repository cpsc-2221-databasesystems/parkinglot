<?php
include 'db.php';
include 'header.php';
?>
  <div id="course" class="container">
		<div class="row">
		<div class="col-12">
			<table class="table table-hover table-striped" id="cursos">
					<thead class="thead-dark">
						<tr>
							<th>Lot ID</th>
							<th>Number of tickets</th>
						</tr>
					</thead>
					<tbody>

    					<?php 
                        $query = "SELECT LID, COUNT(*) AS TicketsIssued FROM Ticket GROUP BY LID ORDER BY TicketsIssued DESC";
                        $query_ticketsperlot = mysqli_query($conexion, $query);
                        
                                  if ( !$query_ticketsperlot ) {
                                        echo "error !!!";
                                        $error_number = mysqli_error( $conexion );
                                        $error_message = mysqli_error( $conexion );
                                        file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                                        http_response_code( 500 );
                                        exit(1);
                                  }                                   
							while($linha = mysqli_fetch_array($query_ticketsperlot)){
								echo '<tr><td >'.$linha['LID'].'</td>';
								echo '<td>'.$linha['TicketsIssued'].'</td></tr>';
							}
  					?>
							
					</tbody>
			</table>
		</div>

		</div> <!--row DIV -->
		
	</div>  <!-- container DIV -->
	
<?php
include 'footer.php'
?>