<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="structured parkinglot database implementation">
  <meta name="author" content="Pablo Soares">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">


  <title>Database Parking</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <!--<link href="css/shop-homepage.css" rel="stylesheet"> -->

</head>

<body>
  <!-- Navigation -->
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="index.php">Database Parking</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="index.php">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Queries
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="freestalls.php">Total number of free stalls</a>
	          <a class="dropdown-item" href="totalcars.php">Total of cars</a>
	          <a class="dropdown-item" href="ticketsprofitsbylid.php">Profits by LID</a>
	          <a class="dropdown-item" href="ticketsissuedbylid.php">Tickets issued group by LID</a>
	          <a class="dropdown-item" href="totalcarsbymakemodel.php">Total of cars group by Make and Model</a>
	          <a class="dropdown-item" href="employeesregistred.php">List of employees</a>
	        </div>
      	  </li>
          <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	          Employee
	        </a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="addemployee.php">Add</a>
	          <a class="dropdown-item" href="employeemanagement.php">Delete/Update</a>
	        </div>
      	  </li>
          <li class="nav-item">
            <a class="nav-link" href="about.php">About
              <span class="sr-only">about</span>
            </a>
          </li>
      	  <li class="nav-item">
      	      <a class="nav-link" href="https://gitlab.com/cpsc-2221-databasesystems/parkinglot.git">GitLab
      	         <img src="img/GitLab_Logo.svg" width="30" height="30" alt="gitlab">
              </a>
      	  </li>
        </ul>
      </div>
    </div>
  </nav> 
<!-- end nav -->  

<!-- header -->
  <header>
		<div class="container">
			<div class="row">
				<!--texto-->
				<div class="col col-12 text-light jumbotron bg-transparent">
					<span class="btn btn-success btn-sm">Starting at $5.99</span>
					<!--a class="btn-sm" reduz o tamanho do botão -->
					<h1>Welcome to Database Parking</h1>
					<p class="text-dark">The best normalized parking in the IT area. Here you can leave your car without worrying about clousure! </p>
					<!--a class="text-dark" torna a cor do texto escura-->
					<a href="index.php#terms" class="btn btn-dark btn-lg">Service terms</a>
					<!--a class="btn-lg" torna grande o tamanho do botão -->
				</div>

			</div>
		</div>
  </header>
	<!-- end header -->
	
</body>
  <!-- Bootstrap core JavaScript -->
  <script src="jquery/jquery-3.4.1.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  
</html>
