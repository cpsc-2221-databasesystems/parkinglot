<?php
include 'db.php';
include 'header.php';
?>
  <div id="course" class="container">
		<div class="row">

		<div class="col-12">
			<table class="table table-hover table-striped" >
					<thead class="thead-dark">
						<tr>
							<th>Total number of free stalls</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$query = "SELECT DISTINCT ((SELECT COUNT(*) FROM Lot_Block_Stall) - (SELECT COUNT(*) FROM Car)) AS TOTAL FROM Lot_Block_Stall";
						$query_availiableStalls = mysqli_query($conexion, $query);

				          if ( ! $query_availiableStalls ) {
				                echo "error !!!";
				                $error_number = mysqli_error( $conexion );
				                $error_message = mysqli_error( $conexion );
				                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
				                http_response_code( 500 );
				                exit(1);
				          } 
							while($linha = mysqli_fetch_array($query_availiableStalls)){
								echo '<tr><td >'.$linha['TOTAL'].'</td>';
								echo '</tr>';
							}
						?>
					</tbody>
			</table>
		</div>

		</div> <!--row DIV -->
		
	</div>  <!-- container DIV -->
	
<?php
include 'footer.php'
?>